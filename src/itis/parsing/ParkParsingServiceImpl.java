package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.*;

public class ParkParsingServiceImpl implements ParkParsingService {


    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        List<ParkParsingException.ParkValidationError> pve = new ArrayList<>();
        BufferedReader br = createBufferedReader(parkDatafilePath);
        Class<Park> parkClass = Park.class;
        try {
            Constructor parkConstructor = parkClass.getDeclaredConstructor();
            parkConstructor.setAccessible(true);
            Park resPark = (Park) parkConstructor.newInstance();
            Field[] declaredFields = parkClass.getDeclaredFields();

            Map<String, String> resMap = fillMap(br);
            for (Field f : declaredFields) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(FieldName.class)) {
                    String fieldValueFromMap = resMap.get(f.getAnnotation(FieldName.class).value());
                    if (f.isAnnotationPresent(NotBlank.class) && (fieldValueFromMap.equals("")||
                            fieldValueFromMap == null)) {
                        pve.add(new ParkParsingException.ParkValidationError(f.getAnnotation(FieldName.class).value(),
                                "Field should not be empty"));
                    } else if (f.isAnnotationPresent(MaxLength.class) &&
                            fieldValueFromMap.length() > f.getAnnotation(MaxLength.class).value()) {
                        pve.add(new ParkParsingException.ParkValidationError(f.getAnnotation(FieldName.class).value(),
                                "Field length must be less than " + f.getAnnotation(MaxLength.class).value()));
                    } else {
                        f.set(resPark, fieldValueFromMap);
                    }
                }else {
                    String fieldName = f.getName();
                    String fieldValueFromMap = resMap.get(fieldName);
                    if (f.isAnnotationPresent(NotBlank.class) && (fieldValueFromMap.equals("")||
                            fieldValueFromMap == null)) {
                        pve.add(new ParkParsingException.ParkValidationError(fieldName,
                                "Field should not be empty"));
                    } else if (f.isAnnotationPresent(MaxLength.class) &&
                            fieldValueFromMap.length() > f.getAnnotation(MaxLength.class).value()) {
                        pve.add(new ParkParsingException.ParkValidationError(fieldName,
                                "Field length must be less than " + f.getAnnotation(MaxLength.class).value()));
                    } else {
                        if(f.getType().equals(LocalDate.class)){
                            f.set(resPark, LocalDate.parse(fieldValueFromMap));
                        }else {
                            f.set(resPark, fieldValueFromMap);
                        }
                    }
                }
                if(!pve.isEmpty()) throw new ParkParsingException("There are errors while parsing " + parkDatafilePath,
                        pve);
                return resPark;
            }
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    private BufferedReader createBufferedReader(String parkDatafilePath) {
        try {
            return new BufferedReader(new FileReader(parkDatafilePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Map<String, String> fillMap(BufferedReader br) {
        Map<String, String> resMap = new HashMap<>();
        try {
            br.readLine();
            String line = br.readLine();
            while (!line.equals("***")) {
                String[] lineArr = line.split(":");
                lineArr[1] = lineArr[1].replace("\"", "").strip();
                lineArr[1] = lineArr[1].equals("null") ? null : lineArr[1];
                resMap.put(lineArr[0].replace("\"", ""), lineArr[1]);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resMap;
    }

}

// try {
//         br.readLine();
//         String line = br.readLine();
//         while (!line.equals("***")) {
//         if(line.contains("title") || line.contains("ownerOrganizationInn") || line.contains("foundationYear")) {
//         String[] lineArr = line.split(":");
//         lineArr[0] = lineArr[0].replace("\"", "");
//         lineArr[1] = lineArr[1].replace("\"", "").strip();
//
//         }
//
//         line = br.readLine();
//         }
//         } catch (IOException e) {
//         e.printStackTrace();
//         }
//         for (Field f : declaredFields) {
//         f.setAccessible(true);
//         f.
//         }